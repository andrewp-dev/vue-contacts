# README #

If you want to run this project, use this commands after cloning  repository please:

**npm install**

**bower install**

### Desktop version ###

![3480073477-joxi_screenshot_1467655177489.png](https://bitbucket.org/repo/5pe99E/images/1345940382-3480073477-joxi_screenshot_1467655177489.png)

### Tablet version ###

![342711855-joxi_screenshot_1467655106809.png](https://bitbucket.org/repo/5pe99E/images/1271668636-342711855-joxi_screenshot_1467655106809.png)

### Mobile version ###

![386024752-joxi_screenshot_1467655031339.png](https://bitbucket.org/repo/5pe99E/images/773474897-386024752-joxi_screenshot_1467655031339.png)
