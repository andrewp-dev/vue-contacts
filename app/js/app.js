var UserList = Vue.extend({
    name: 'userlist'
});

var vm = new UserList({
    el: '.container',
    data: {
        folder: 'img/avatars/',
        expansion: '.svg',

        /*----------- User info -----------*/

        users:  [],
        activeUser: [],

        /*------- Sort parameters --------*/

        search:  '',
        sortByValue:  '',
        sortDirection: 0,
        isOrderByName: true,
        isOrderByAge:  true,
    },

    created: function () {
        this.requestJSONParse();
        this.setAvatarsURL();
        this.getActiveUser();
    },

    methods: {
        requestJSONParse: function () {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'data/contacts.json', false);
            xhr.send();

            if (xhr.status != 200) {
                alert( xhr.status + ': ' + xhr.statusText );  // пример вывода: 404: Not Found
            } else {                                         // вывести результат
                this.users = JSON.parse(xhr.responseText);  // responseText -- текст ответа.
            }
        },

        setAvatarsURL: function () {
            for(var i = 0; i < this.users.length; i++) {
                this.users[i].avatar = this.folder + this.users[i].avatar + this.expansion;
            }
        },

        getActiveUser: function (user) {
            user ? this.activeUser = user : this.activeUser = this.users[0];
        },

        sortBy: function (value) {
            this.sortByValue = value;

            if (this.sortDirection == 0) {
                this.sortDirection++;

            } else if (this.sortDirection ==  1 || this.sortDirection == -1) {
                if (value == 'age') {
                    this.isOrderByAge == true ? this.isOrderByAge = false : this.isOrderByAge = true;

                } else if (value == 'name.first') {
                    this.isOrderByName == true ? this.isOrderByName = false : this.isOrderByName = true;
                }

                this.sortDirection *= -1;
            }
        },

        resetSortParameters: function () {
            this.isOrderByName = true;
            this.isOrderByAge  = true;
            this.sortDirection = 0;
            this.search        = '';
        },
    },

    computed: {
        userName: function () {
            return this.activeUser.name.first + ' ' + this.activeUser.name.last;
        }
    },

});